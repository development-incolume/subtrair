import sys
import pytest
from collections import Iterable

def subtrair(*args):
    lista = list()
    try:
        if len(args) == 1 and isinstance(args[0], Iterable):
            for i in range(len(args[0])):
                if i == 0:
                    lista.append(float(args[0][i]))
                else:
                    lista.append(-float(args[0][i]))
            pass
        else:
            for i in range(len(args)):
                if i == 0:
                    lista.append(float(args[i]))
                else:
                    lista.append(-float(args[i]))
        return sum(lista)
        pass
    except ValueError:
        raise
    except:
        print(sys.exc_info())
    pass

def test_num():
    assert subtrair() == 0
    assert subtrair(1, 2) == -1
    assert subtrair(0, 0) == 0
    assert subtrair(0, 1) == -1
    assert subtrair(-10, 5) == -15
    assert subtrair(1.9999, .0001) == 1.9998
    assert subtrair([9, 8]) == 1.
    assert subtrair((9, 8)) == 1.
    pass

def test_str():
    assert subtrair('1', '9') == -8
    assert subtrair('2.5', '1.50') == 1
    assert subtrair('-10', '-5', '5.9', .1) == -11.0
    with pytest.raises(ValueError) as e:
        subtrair('a', 'b')
        e.match(r"could not convert string to float")
    with pytest.raises(ValueError) as e:
        subtrair([1, 2, 'b'])
        e.match(r"could not convert string to float")
    assert subtrair(('9', '8')) == 1.
    pass



if __name__ == '__main__':
    print(subtrair(3,2))
    pass